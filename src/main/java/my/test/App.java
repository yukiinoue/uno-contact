package my.test;

import ooo.connector.BootstrapSocketConnector;

import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.XComponentContext;

public class App
{
    public static void main( String[] args )
    {
    	try {
    	    // the example code from
            // https://wiki.openoffice.org/wiki/Documentation/DevGuide/FirstSteps/First_Contact
            // does not work
//    	    XComponentContext xContext = com.sun.star.comp.helper.Bootstrap.bootstrap();
    	    String oooFolder = "/opt/homebrew-cask/Caskroom/openoffice/4.1.1/OpenOffice.app/Contents/MacOS/";
    	    XComponentContext xContext = BootstrapSocketConnector.bootstrap(oooFolder);

    	    System.out.println("Connected to a running office ...");

    	    XMultiComponentFactory xMCF = xContext.getServiceManager();

            String available = (xMCF != null ? "available" : "not available");
    	    System.out.println("remote ServiceManager is " + available);
    	} catch (java.lang.Exception e) {
    	    e.printStackTrace();
    	} finally {
    	    System.exit(0);
    	}
    }
}
